<?php include('includes/header.php');?>
<?php include('includes/navigation.php');?>

<section class="content">
	<div class="container">
    <div class="row">
      <div class="col-md-12">
      	<h1>Bus Detail</h1>
      	<div class="row">
          <div class="col-md-4 col-sm-4">
            <div class="bus-item bus-single-item">
              <div class="bus-img" style="background-image: url('images/bus-img1.jpg');">                
                <a href="bus-detail.php"></a>
              </div>
            </div>
          </div> 
          <?php 
            $busNo = $_GET['busNo'];
            $busRoute = $_GET['busRoute'];
            $busLocation = $_GET['busLoc'];

          ?>
          <div class="col-md-8 col-sm-8">
            <div class="bus-detail">
              <div class="bus-info">
                <h2><?php echo strtoupper( $busNo ); ?></h2>
                <p><span>Current Location:</span> <?php echo $busRoute; ?></p>
                <p><span>Route:</span> <?php echo $busLocation; ?></p>
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc placerat risus ac augue pulvinar pellentesque. Donec vitae lacinia mi. Cras vel pretium mi, nec egestas dui. Fusce euismod ligula nec erat semper posuere. Curabitur ac ipsum elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur viverra vehicula felis. Phasellus blandit gravida nisi quis rutrum. Cras id efficitur erat, in vehicula nulla. In dignissim justo ac justo pharetra gravida.</p>
            </div>
          </div>
        </div>        

        <div class="google-map">
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d56516.31397712412!2d85.3261328!3d27.708960349999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2snp!4v1426676523087" width="100%" height="100%" frameborder="0" style="border:0"></iframe>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include('includes/footer.php');?>    