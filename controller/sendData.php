<?php 
/*
This script recieves the query from the Client App and parses the request 
to send the result. The expected data is as follows : 

> Bus ID
> Lat of the Client device
> Lon of the Client device

Also, in JSON format. The query is processed and compared with the DB and
sent again in JSON format. 

The processing will be composed of 
a) finding the nearest bus 
b) How long will will the vehicle take to reach to Client's location

Started date : 27 Feb 2015
Time estimated : 12.5 hours  */


?>

