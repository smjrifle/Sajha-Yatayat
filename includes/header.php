<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Sajha</title>

  <link rel="icon" href="images/favicon.ico">

  <link rel="stylesheet" href="css/jquery.datetimepicker.css">

  <link href="less/style.less" rel="stylesheet/less">
  <!-- Bootstrap -->
  <script src="js/funcions.js"></script>
  <script src="js/less-1.7.5.js"></script>
  <link rel="stylesheet" type="text/css" href="css/sweet-alert.css">

  <script src="http://maps.google.com/maps/api/js?v=3&sensor=false" type="text/javascript"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->


    </head>
    <!-- Connecting to database -->
    <?php 
          include("config/connectDB.php");
    ?>
        <body>