
var center = null;
var map = null;
var currentPopup;
var bounds = new google.maps.LatLngBounds();
var request = null;
    var markers = []; //marker array 
    var request;

    var icon = new google.maps.MarkerImage(
      "http://maps.google.com/mapfiles/ms/micons/blue.png",
      new google.maps.Size(32, 32)
      );  /*requesting for marker image*/


    $(window).resize(function() {
    //spinner width
    var google_map_width = $('.google-map').width();
    $('.spinner-wrap').css('width', google_map_width);
  });

    $(document).ready(function() {
    //spinner width
    var google_map_width = $('.google-map').width();
    display_spinner();
    $('.spinner-wrap').css('width', google_map_width);
    $('#fromT').datetimepicker({
      datepicker:false,
      format:'H:i',
      step:10
    });



    $('#toT').datetimepicker({
      datepicker:false,
      format:'H:i',
      step:10
    });


    
    map = new google.maps.Map(document.getElementById("map"), {
     center: new google.maps.LatLng(25, 86),
     zoom: 7,
     mapTypeId: google.maps.MapTypeId.ROADMAP,
     mapTypeControl: false,
     mapTypeControlOptions: {
       style: google.maps.MapTypeControlStyle.VERTICAL_BAR
     },
     navigationControl: true,
     navigationControlOptions: {
       style: google.maps.NavigationControlStyle.SMALL
     }
   });



    var data =  "fromTime="+'i'+"&toTime="+'i' ;

    if (request) {
      request.abort();
    }
    request = $.ajax({
      url: "controller/indexController/getVehicleInfo.php",
      type: "post",
      data: data
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console

        var res = eval('(' + response + ')') ;


        if(res.length > 1) {

         for(var i=0;i<res.length;i++){  // showing in map

           addMarker(res[i]["lat"],res[i]["lon"],res[i]["route"],res[i]["name"],res[i]["time"]);

         }
       center = bounds.getCenter();  // find the center 
       map.fitBounds(bounds);  // make the boundary and zoom in
       bounds = new google.maps.LatLngBounds(); // 
       window.setTimeout(hide_spinner,3000);
     }
     else {
      window.setTimeout(hide_spinner,0);
      swal(
      {
        title: "No entry in the database",
        timer: 1500,
        showConfirmButton: false
      });

    }


  });
    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
          "The following error occurred: "+
          textStatus, errorThrown
          );
      });


    $("#form").submit(function(event){

    // Abort any pending request
    event.preventDefault();
    display_spinner();

    if (request) {
      request.abort();
    }
    // setup some local variables
    data = "";
    var fDate = $('#fromT').datetimepicker('getTime')[0]['value'];
    var tDate = $('#toT').datetimepicker('getTime')[0]['value'];
    var VehicleID = $( "#vehicleReg option:selected" ).text();
    var VehicleRoute = $( "#vehicleRoute option:selected" ).text();
    var date="";

    deleteMarkers();

      if(VehicleRoute || VehicleID ){             // Handling all case
        if ( VehicleID ){
          if(VehicleID.toLowerCase() == 'all' )
            VehicleID = '*';

          VehicleID = "&deviceID="+VehicleID;
        }

        if ( VehicleRoute ){
          if(VehicleRoute.toLowerCase() == 'all' )
            VehicleRoute = '*';
          VehicleRoute = "&route="+VehicleRoute;
        }
      }

      if(tDate && fDate) {
        if(fDate < tDate )
          date = "fromTime="+fDate+"&toTime="+tDate;
        else 
          alert("Date selection error");

      }
      
      data = date + VehicleID + VehicleRoute;
      console.log(data);

      

      request = $.ajax({
        url: "controller/indexController/getVehicleInfo.php",
        type: "post",
        data: data
      });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        var data =  eval('(' + response + ')');
        if(data.length > 1){
     for(var i=0;i<data.length;i++){  // showing in map
      if(i != data.length-1){
        if(distance(data[i]["lat"],data[i]["lon"],data[i+1]["lat"],data[i+1]["lon"]) >= 0.05 && 
          distance(data[i]["lat"],data[i]["lon"],data[i+1]["lat"],data[i+1]["lon"]) < 0.5){
         addMarker(data[i]["lat"],data[i]["lon"],data[i]["route"],data[i]["name"],data[i]["time"]);
     }  
   }
 }
       center = bounds.getCenter();  // find the center 
       map.fitBounds(bounds);  // make the boundary and zoom in
       bounds = new google.maps.LatLngBounds(); // 

       window.setTimeout(hide_spinner,3000);
     }
     else {
       window.setTimeout(hide_spinner,0);
       swal(
       {
        title: "No entry in the database",
        timer: 1500,
        showConfirmButton: false
      });
       
     }



   });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
          "The following error occurred: "+
          textStatus, errorThrown
          );
      });



  });

google.maps.event.addDomListener(window, 'load', initialize);

});