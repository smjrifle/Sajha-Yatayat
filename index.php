<?php include('includes/header.php');?>
<?php include('includes/navigation.php');?>

<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="filter clearfix">
          <form action="" id = "form" >
            <div class="filter-item">
              <label for="fromT">Start Time</label>
              <input type="text" value="" id="fromT">
            </div>
            <div class="filter-item">
              <label for="toT">End Time</label>
              <input type="text" value="" id="toT">
            </div>
            <div class="filter-item">
              <label for="fromT">Bus Number</label>
              <select id="vehicleReg">         
                <option value="*" selected="selected">All</option>
                <?php 
                $query = mysql_query("select distinct(deviceID) from yatayat");
                if(mysql_num_rows($query) >= 1){
                  while ($row = mysql_fetch_array($query)){
                    echo "<option>";
                    echo $row["deviceID"];
                    echo "</option>";
                  }
                }
                ?>        
              </select>
            </div>
            <div class="filter-item">
              <label for="fromT">Route</label>
              <select id="vehicleRoute">         
                <option value="*" selected="selected">All</option>
                <?php 
                $query = mysql_query("select distinct(route) from yatayat");
                if(mysql_num_rows($query) >= 1){
                  while ($row = mysql_fetch_array($query)){
                    echo "<option>";
                    echo $row["route"];
                    echo "</option>";
                  }
                }       
                ?>
              </select>
            </div>
            <div class="filter-item">
              <input type="submit" value="Submit" >
            </div>
          </form>
        </div>
        <div class="google-map google-map-wrapper">
          <div class="spinner-wrap" style="display: block;">
            <div class="spinner">
              <img src="images/bus.gif" alt=""><br>
              Loading, Please wait...
            </div>
          </div>
          <div class="google-map" id="map" > 
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include('includes/footer.php');?>    