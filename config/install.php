<?php
// Connect to MySQL
include("connectDB.php");

$msg = "";

if (!$db_selected) {
  // If we couldn't, then it either doesn't exist, or we can't see it.
  $sql = 'CREATE DATABASE '.$dbName;

  if (mysql_query($sql, $link)) {
      $msg = "Database ".$dbName. " created successfully\n";

  } else {
      $msg = 'Error creating database: ' . mysql_error() . "\n";
  }
}

$db_selected = mysql_select_db($dbName, $link);

  $sql = "SHOW TABLES FROM {$dbName};";
  $result = mysql_query($sql);

if (!$result) {
    echo "DB Error, could not list tables\n";
    echo 'MySQL Error: ' . mysql_error();
    die();
}

$createTable = 1;
while ($row = mysql_fetch_row($result)) {
    if($row[0] == $dbName){
    	$createTable = 1;
    	break;
    	}

    else
    	$createTable = 0;
}

	if($createTable){
  $sql = "CREATE TABLE IF NOT EXISTS ". "`".  $tableName ."`"." (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `status` varchar(100) NOT NULL,
  `route` varchar(100) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lon` varchar(100) NOT NULL,
  `speed` varchar(100) NOT NULL,
  `timesStamp` datetime NOT NULL,
  `deviceID` varchar(100) NOT NULL,
  `androidID` varchar(200) NULL,
  `internetBalance` varchar(200) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";

 if (mysql_query($sql)) {
      $msg = $tableName. " created successfully in " . $dbName ."\n";
  } else {
      $msg = 'Error creating table: ' . mysql_error() . "\n";
  }
}
	else{
      $msg =  'Table already created';
	}

  echo $msg;

mysql_close($link);
