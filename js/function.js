 

function addMarker(lat, lng, route, name, time) {

   var pt = new google.maps.LatLng(lat, lng);
   bounds.extend(pt);
   var marker = new google.maps.Marker({
    position: pt,
    icon: icon,
    map: map
});

   var popup = new google.maps.InfoWindow({
    content: route + " " + name + "\n" + time,
    maxWidth: 300
});

   google.maps.event.addListener(marker, "click", function() {
    if (currentPopup != null) {
        currentPopup.close();
        currentPopup = null;
    }

    popup.open(map, marker);
    currentPopup = popup;
});

   google.maps.event.addListener(popup, "closeclick", function() {
    map.panTo(center);
    currentPopup = null;
});
   markers.push(marker);

}


    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setAllMap(null);
  }   


     // Sets the map on all markers in the array.
     function setAllMap(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

        // Shows any markers currently in the array.
        function showMarkers() {
          setAllMap(map);
      }

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

//spinner loader
function display_spinner() {
  // alert('spinner');
  $('.spinner-wrap').fadeIn(1000);  
  
}
function hide_spinner() {
  //alert('spinner');
  $('.spinner-wrap').fadeOut(1000);  
  
}
function initialize(){  
        /* display_spinner();
         map = new google.maps.Map(document.getElementById("map"), {
         center: new google.maps.LatLng(25, 86),
         zoom: 7,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
         mapTypeControl: false,
         mapTypeControlOptions: {
         style: google.maps.MapTypeControlStyle.VERTICAL_BAR
         },
         navigationControl: true,
         navigationControlOptions: {
         style: google.maps.NavigationControlStyle.SMALL
                }
            });*/

 
}


function distance(lat1, lon1, lat2, lon2) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var radlon1 = Math.PI * lon1/180
    var radlon2 = Math.PI * lon2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    return dist * 1.609344 
}                                                                           