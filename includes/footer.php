		<footer>
			<div class="container">
				<p class="pull-right">Powered by: <a href="http://kazistudios.com" target="_blank">Kazi Studios</a></p>
			</div>	
		</footer>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/sweet-alert.min.js"></script>
    <script src="js/jquery.datetimepicker.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/function.js"></script>
    
  </body>
</html>